﻿
namespace DemoApp
{
  partial class FormUser
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private global::System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.menuStrip1 = new System.Windows.Forms.MenuStrip();
      this.mnSwitchAd = new System.Windows.Forms.ToolStripMenuItem();
      this.splitContainer1 = new System.Windows.Forms.SplitContainer();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.btnCreate = new System.Windows.Forms.Button();
      this.panel2 = new System.Windows.Forms.Panel();
      this.cboType = new System.Windows.Forms.ComboBox();
      this.label3 = new System.Windows.Forms.Label();
      this.panel1 = new System.Windows.Forms.Panel();
      this.txtAmount = new NumericTextBox();
      this.label2 = new System.Windows.Forms.Label();
      this.pnName = new System.Windows.Forms.Panel();
      this.txtName = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.splitContainer2 = new System.Windows.Forms.SplitContainer();
      this.splitContainer3 = new System.Windows.Forms.SplitContainer();
      this.txtSearch = new System.Windows.Forms.TextBox();
      this.btnSearch = new System.Windows.Forms.Button();
      this.lstAccount = new System.Windows.Forms.ListView();
      this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
      this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
      this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
      this.menuStrip1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
      this.splitContainer1.Panel1.SuspendLayout();
      this.splitContainer1.Panel2.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      this.groupBox1.SuspendLayout();
      this.panel2.SuspendLayout();
      this.panel1.SuspendLayout();
      this.pnName.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
      this.splitContainer2.Panel1.SuspendLayout();
      this.splitContainer2.Panel2.SuspendLayout();
      this.splitContainer2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
      this.splitContainer3.Panel1.SuspendLayout();
      this.splitContainer3.Panel2.SuspendLayout();
      this.splitContainer3.SuspendLayout();
      this.SuspendLayout();
      // 
      // menuStrip1
      // 
      this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnSwitchAd});
      this.menuStrip1.Location = new System.Drawing.Point(0, 0);
      this.menuStrip1.Name = "menuStrip1";
      this.menuStrip1.Size = new System.Drawing.Size(584, 24);
      this.menuStrip1.TabIndex = 0;
      this.menuStrip1.Text = "menuStrip1";
      // 
      // mnSwitchAd
      // 
      this.mnSwitchAd.Name = "mnSwitchAd";
      this.mnSwitchAd.Size = new System.Drawing.Size(93, 20);
      this.mnSwitchAd.Text = "Switch Admin";
      this.mnSwitchAd.Click += new System.EventHandler(this.mnSwitchAd_Click);
      // 
      // splitContainer1
      // 
      this.splitContainer1.Cursor = System.Windows.Forms.Cursors.Default;
      this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer1.IsSplitterFixed = true;
      this.splitContainer1.Location = new System.Drawing.Point(0, 24);
      this.splitContainer1.Name = "splitContainer1";
      // 
      // splitContainer1.Panel1
      // 
      this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
      // 
      // splitContainer1.Panel2
      // 
      this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
      this.splitContainer1.Size = new System.Drawing.Size(584, 387);
      this.splitContainer1.SplitterDistance = 194;
      this.splitContainer1.TabIndex = 1;
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.btnCreate);
      this.groupBox1.Controls.Add(this.panel2);
      this.groupBox1.Controls.Add(this.panel1);
      this.groupBox1.Controls.Add(this.pnName);
      this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox1.Location = new System.Drawing.Point(0, 0);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(194, 387);
      this.groupBox1.TabIndex = 0;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Create save account";
      // 
      // btnCreate
      // 
      this.btnCreate.Dock = System.Windows.Forms.DockStyle.Top;
      this.btnCreate.Location = new System.Drawing.Point(3, 161);
      this.btnCreate.Name = "btnCreate";
      this.btnCreate.Size = new System.Drawing.Size(188, 23);
      this.btnCreate.TabIndex = 3;
      this.btnCreate.Text = "Create";
      this.btnCreate.UseVisualStyleBackColor = true;
      this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
      // 
      // panel2
      // 
      this.panel2.Controls.Add(this.cboType);
      this.panel2.Controls.Add(this.label3);
      this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel2.Location = new System.Drawing.Point(3, 129);
      this.panel2.Name = "panel2";
      this.panel2.Padding = new System.Windows.Forms.Padding(3);
      this.panel2.Size = new System.Drawing.Size(188, 32);
      this.panel2.TabIndex = 2;
      // 
      // cboType
      // 
      this.cboType.Dock = System.Windows.Forms.DockStyle.Right;
      this.cboType.FormattingEnabled = true;
      this.cboType.Location = new System.Drawing.Point(64, 3);
      this.cboType.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
      this.cboType.Name = "cboType";
      this.cboType.Size = new System.Drawing.Size(121, 23);
      this.cboType.TabIndex = 1;
      // 
      // label3
      // 
      this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
      this.label3.Location = new System.Drawing.Point(3, 3);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(182, 26);
      this.label3.TabIndex = 0;
      this.label3.Text = "Type:";
      this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.txtAmount);
      this.panel1.Controls.Add(this.label2);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel1.Location = new System.Drawing.Point(3, 74);
      this.panel1.Name = "panel1";
      this.panel1.Padding = new System.Windows.Forms.Padding(3);
      this.panel1.Size = new System.Drawing.Size(188, 55);
      this.panel1.TabIndex = 1;
      // 
      // txtAmount
      // 
      this.txtAmount.Dock = System.Windows.Forms.DockStyle.Fill;
      this.txtAmount.EnableInputDot = true;
      this.txtAmount.Location = new System.Drawing.Point(3, 18);
      this.txtAmount.Name = "txtAmount";
      this.txtAmount.PlaceholderText = "Input number in dollar($)";
      this.txtAmount.Size = new System.Drawing.Size(182, 23);
      this.txtAmount.TabIndex = 1;
      // 
      // label2
      // 
      this.label2.Dock = System.Windows.Forms.DockStyle.Top;
      this.label2.Location = new System.Drawing.Point(3, 3);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(182, 15);
      this.label2.TabIndex = 0;
      this.label2.Text = "Amount:";
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // pnName
      // 
      this.pnName.Controls.Add(this.txtName);
      this.pnName.Controls.Add(this.label1);
      this.pnName.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnName.Location = new System.Drawing.Point(3, 19);
      this.pnName.Name = "pnName";
      this.pnName.Padding = new System.Windows.Forms.Padding(3);
      this.pnName.Size = new System.Drawing.Size(188, 55);
      this.pnName.TabIndex = 0;
      // 
      // txtName
      // 
      this.txtName.Dock = System.Windows.Forms.DockStyle.Fill;
      this.txtName.Location = new System.Drawing.Point(3, 18);
      this.txtName.Name = "txtName";
      this.txtName.Size = new System.Drawing.Size(182, 23);
      this.txtName.TabIndex = 1;
      // 
      // label1
      // 
      this.label1.Dock = System.Windows.Forms.DockStyle.Top;
      this.label1.Location = new System.Drawing.Point(3, 3);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(182, 15);
      this.label1.TabIndex = 0;
      this.label1.Text = "Name:";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // splitContainer2
      // 
      this.splitContainer2.Cursor = System.Windows.Forms.Cursors.Default;
      this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer2.Location = new System.Drawing.Point(0, 0);
      this.splitContainer2.Name = "splitContainer2";
      this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer2.Panel1
      // 
      this.splitContainer2.Panel1.Controls.Add(this.splitContainer3);
      this.splitContainer2.Panel1.Padding = new System.Windows.Forms.Padding(5);
      // 
      // splitContainer2.Panel2
      // 
      this.splitContainer2.Panel2.Controls.Add(this.lstAccount);
      this.splitContainer2.Panel2.Padding = new System.Windows.Forms.Padding(5);
      this.splitContainer2.Size = new System.Drawing.Size(386, 387);
      this.splitContainer2.SplitterDistance = 35;
      this.splitContainer2.TabIndex = 0;
      // 
      // splitContainer3
      // 
      this.splitContainer3.Cursor = System.Windows.Forms.Cursors.Default;
      this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer3.Location = new System.Drawing.Point(5, 5);
      this.splitContainer3.Name = "splitContainer3";
      // 
      // splitContainer3.Panel1
      // 
      this.splitContainer3.Panel1.Controls.Add(this.txtSearch);
      // 
      // splitContainer3.Panel2
      // 
      this.splitContainer3.Panel2.Controls.Add(this.btnSearch);
      this.splitContainer3.Size = new System.Drawing.Size(376, 25);
      this.splitContainer3.SplitterDistance = 300;
      this.splitContainer3.TabIndex = 0;
      // 
      // txtSearch
      // 
      this.txtSearch.Dock = System.Windows.Forms.DockStyle.Fill;
      this.txtSearch.Location = new System.Drawing.Point(0, 0);
      this.txtSearch.Name = "txtSearch";
      this.txtSearch.PlaceholderText = "Search account";
      this.txtSearch.Size = new System.Drawing.Size(300, 23);
      this.txtSearch.TabIndex = 0;
      // 
      // btnSearch
      // 
      this.btnSearch.Dock = System.Windows.Forms.DockStyle.Top;
      this.btnSearch.Location = new System.Drawing.Point(0, 0);
      this.btnSearch.Name = "btnSearch";
      this.btnSearch.Size = new System.Drawing.Size(72, 23);
      this.btnSearch.TabIndex = 1;
      this.btnSearch.Text = "Search";
      this.btnSearch.UseVisualStyleBackColor = true;
      this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
      // 
      // lstAccount
      // 
      this.lstAccount.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
      this.lstAccount.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lstAccount.HideSelection = false;
      this.lstAccount.Location = new System.Drawing.Point(5, 5);
      this.lstAccount.Name = "lstAccount";
      this.lstAccount.Size = new System.Drawing.Size(376, 338);
      this.lstAccount.TabIndex = 0;
      this.lstAccount.UseCompatibleStateImageBehavior = false;
      this.lstAccount.View = System.Windows.Forms.View.Details;
      // 
      // columnHeader1
      // 
      this.columnHeader1.Text = "Name";
      this.columnHeader1.Width = 150;
      // 
      // columnHeader2
      // 
      this.columnHeader2.Text = "Amount";
      this.columnHeader2.Width = 100;
      // 
      // columnHeader3
      // 
      this.columnHeader3.Text = "Type";
      this.columnHeader3.Width = 100;
      // 
      // FormUser
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
      this.ClientSize = new System.Drawing.Size(584, 411);
      this.Controls.Add(this.splitContainer1);
      this.Controls.Add(this.menuStrip1);
      this.MainMenuStrip = this.menuStrip1;
      this.Name = "FormUser";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "User";
      this.menuStrip1.ResumeLayout(false);
      this.menuStrip1.PerformLayout();
      this.splitContainer1.Panel1.ResumeLayout(false);
      this.splitContainer1.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
      this.splitContainer1.ResumeLayout(false);
      this.groupBox1.ResumeLayout(false);
      this.panel2.ResumeLayout(false);
      this.panel1.ResumeLayout(false);
      this.panel1.PerformLayout();
      this.pnName.ResumeLayout(false);
      this.pnName.PerformLayout();
      this.splitContainer2.Panel1.ResumeLayout(false);
      this.splitContainer2.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
      this.splitContainer2.ResumeLayout(false);
      this.splitContainer3.Panel1.ResumeLayout(false);
      this.splitContainer3.Panel1.PerformLayout();
      this.splitContainer3.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
      this.splitContainer3.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private global::System.Windows.Forms.MenuStrip menuStrip1;
    private global::System.Windows.Forms.ToolStripMenuItem mnSwitchAd;
    private global::System.Windows.Forms.SplitContainer splitContainer1;
    private global::System.Windows.Forms.GroupBox groupBox1;
    private global::System.Windows.Forms.Button btnCreate;
    private global::System.Windows.Forms.Panel panel2;
    private global::System.Windows.Forms.ComboBox cboType;
    private global::System.Windows.Forms.Label label3;
    private global::System.Windows.Forms.Panel panel1;
    private global::System.Windows.Forms.Label label2;
    private global::System.Windows.Forms.Panel pnName;
    private global::System.Windows.Forms.TextBox txtName;
    private global::System.Windows.Forms.Label label1;
    private global::System.Windows.Forms.SplitContainer splitContainer2;
    private global::System.Windows.Forms.TextBox txtSearch;
    private global::System.Windows.Forms.ListView lstAccount;
    private global::System.Windows.Forms.Button btnSearch;
    private global::System.Windows.Forms.SplitContainer splitContainer3;
    private global::System.Windows.Forms.ColumnHeader columnHeader1;
    private global::System.Windows.Forms.ColumnHeader columnHeader2;
    private global::System.Windows.Forms.ColumnHeader columnHeader3;
    private NumericTextBox txtAmount;
  }
}