﻿using DemoApp.Model;
using InterestRateSystem;
using InterestRateSystem.System;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static DemoApp.Model.MyCustomer;

namespace DemoApp
{
  public partial class FormInterestRate : Form
  {
    string textInterest = "Interest Rate:";
    List<MyRate> dataRate = MyInterestSystem.Instance().Rates.OrderBy(rate => rate.Month).ToList();
    public FormInterestRate()
    {
      InitializeComponent();
      txtRate.Text = $"{textInterest} 0%";
      loadCombobox();
      cboAccType.DataSource = Enum.GetValues(typeof(CustomerType));
      cboAccType.SelectedIndex = 0;
    }
    private void loadCombobox()
    {
      cboRType.DataSource = (from rate in dataRate select new {MonthNum = rate.Month, Rate = $"{rate.InterestRate * 100}(%)", Month = Helper.intMonthToString(rate.Month) }).ToList();
      
      cboRType.DisplayMember = "Month";
      cboRType.ValueMember = "MonthNum";
      cboRType.SelectedItem = 0;
    }

    private void btnCalculate_Click(object sender, EventArgs e)
    {
      if (string.IsNullOrEmpty(txtAmount.Text))
      {
        MessageBox.Show("Please fill Amount data", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        txtAmount.Focus();
        return;
      }
      int months = int.Parse(cboRType.SelectedValue.ToString());
      float amount = float.Parse(txtAmount.Text);
      CustomerType cType = Enum.Parse<CustomerType>(cboAccType.SelectedValue.ToString());
      InterestRateCal.Instance().AttachCustomerProvider(new MyCustomerInfoProvider());
      (float IRateAmount, float BBigAmount, float BonusOther, float Total) amountData =
        InterestRateCal.Instance().Calculate(InterestRateRepository.findBonusForAccountType(cType), months, amount);
      
      
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.AppendLine($"Amount: {amount}$");
      stringBuilder.AppendLine($"Interest Rate Amount: {amountData.IRateAmount}$");
      if(amountData.BBigAmount!=0) stringBuilder.AppendLine($"Bonus Big Amount: {amountData.BBigAmount}$");
      
      stringBuilder.AppendLine($"Bonus Account Type: {amountData.BonusOther}$");
      stringBuilder.AppendLine($"Total: {amountData.Total}$");
      txtResult.Text = stringBuilder.ToString();
      pnResult.Visible = true;
      
    }

    private void cboType_SelectedIndexChanged(object sender, EventArgs e)
    {
      pnResult.Visible = false;
      int month; 
      if(int.TryParse(cboRType.SelectedValue.ToString(), out month)){
        MyRate rate = dataRate.Where(rate => rate.Month == month).First();
        txtRate.Text = $"{textInterest} {rate.InterestRate * 100}%";
      }
    }

    private void txtAmount_TextChanged(object sender, EventArgs e)
    {
      pnResult.Visible = false;
    }

    private void cboRType_ValueMemberChanged_1(object sender, EventArgs e)
    {
      MyRate rate = dataRate.Where(rate => rate.Month == int.Parse(cboRType.SelectedValue.ToString())).First();
      txtRate.Text = $"{textInterest} {rate.InterestRate * 100}%";
    }

    private void cboAccType_SelectedIndexChanged(object sender, EventArgs e)
    {
      pnResult.Visible = false;
    }
  }
}
