﻿using DemoApp.Model;
using InterestRateSystem;
using InterestRateSystem.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DemoApp.Model.MyCustomer;

namespace DemoApp
{
  internal class InterestRateRepository
  {
    public static MyRate findInterestRate(int month)
    {
      MyRate find = MyInterestSystem.Instance().Rates.Where(rate => rate.Month == month).First();
      return find;
    }
    public static MyBonus findBonusRateByRateIdAndAmount(int month, float amount)
    {
      //Bonus lower = MyInterestSystem.Instance().Bonus.findLower(new MyBonus() { Month = month, Amount = amount }, (a, b) => {
      //  if (a.Month <= b.Month && a.Amount <= b.Amount) return 0;
      //  else return -1;
      //});
      MyBonus lower =null;
      List<MyBonus> lowerList = MyInterestSystem.Instance().Bonus.toList().Where(bonus => bonus.Month <= month).OrderBy(bonus => bonus.Amount).ToList();
      findLowerBinary(lowerList, 0, lowerList.Count - 1, ref lower, amount);
      return lower;
    }
    private static void findLowerBinary(List<MyBonus> data, int start, int end, ref MyBonus cBonus, float amount)
    {
      if (start < end)
      {
        int mid = (start + end) / 2;
        if (data[mid].Amount <= amount)
        {
          cBonus = data[mid];
          findLowerBinary(data, mid + 1, end, ref cBonus, amount);
        }
        else
        {
          findLowerBinary(data, start, mid - 1, ref cBonus, amount);
        }
      }
      else if (start == end)
      {
        if (data[start].Amount <= amount)
        {
          cBonus = data[start];
        }
      }
    }
    public static MyCustomer findBonusForAccountType(CustomerType cType)
    {
      MyCustomer customerType;
      switch (cType)
      {
        case CustomerType.Personal:
          customerType = new PersonalCustomer();
          break;
        case CustomerType.Company:
          customerType = new CompanyCustomer();
          break;
        default:
          customerType = new CoperativeBankCustomer();
          break;
      }
      return customerType;
    }
  }
}
