﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoApp
{
  public partial class FormMain : Form
  {
    public FormMain()
    {
      InitializeComponent();
    }

    private void btnAdminForm_Click(object sender, EventArgs e)
    {
      FormAdmin formAdmin = new FormAdmin();
      formAdmin.Show();
    }

    private void btnUserForm_Click(object sender, EventArgs e)
    {
      FormUser formUser = new FormUser();
      formUser.Show();
    }

    private void btnCalRate_Click(object sender, EventArgs e)
    {
      
    }

    private void btnCalRate_Click_1(object sender, EventArgs e)
    {
      FormInterestRate formInterestRate = new FormInterestRate();
      formInterestRate.Show();
    }
  }
}
