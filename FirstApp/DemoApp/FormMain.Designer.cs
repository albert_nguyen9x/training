﻿
namespace DemoApp
{
  partial class FormMain
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private global::System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.splitContainer1 = new System.Windows.Forms.SplitContainer();
      this.btnAdminForm = new System.Windows.Forms.Button();
      this.btnCalRate = new System.Windows.Forms.Button();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
      this.splitContainer1.Panel1.SuspendLayout();
      this.splitContainer1.Panel2.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      this.SuspendLayout();
      // 
      // splitContainer1
      // 
      this.splitContainer1.Cursor = System.Windows.Forms.Cursors.Default;
      this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer1.IsSplitterFixed = true;
      this.splitContainer1.Location = new System.Drawing.Point(30, 30);
      this.splitContainer1.Name = "splitContainer1";
      this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer1.Panel1
      // 
      this.splitContainer1.Panel1.Controls.Add(this.btnAdminForm);
      this.splitContainer1.Panel1.Cursor = System.Windows.Forms.Cursors.Default;
      this.splitContainer1.Panel1.Padding = new System.Windows.Forms.Padding(5);
      // 
      // splitContainer1.Panel2
      // 
      this.splitContainer1.Panel2.Controls.Add(this.btnCalRate);
      this.splitContainer1.Panel2.Padding = new System.Windows.Forms.Padding(5);
      this.splitContainer1.Size = new System.Drawing.Size(324, 123);
      this.splitContainer1.SplitterDistance = 60;
      this.splitContainer1.TabIndex = 0;
      // 
      // btnAdminForm
      // 
      this.btnAdminForm.Dock = System.Windows.Forms.DockStyle.Fill;
      this.btnAdminForm.Location = new System.Drawing.Point(5, 5);
      this.btnAdminForm.Name = "btnAdminForm";
      this.btnAdminForm.Size = new System.Drawing.Size(314, 50);
      this.btnAdminForm.TabIndex = 0;
      this.btnAdminForm.Text = "Open Admin";
      this.btnAdminForm.UseVisualStyleBackColor = true;
      this.btnAdminForm.Click += new System.EventHandler(this.btnAdminForm_Click);
      // 
      // btnCalRate
      // 
      this.btnCalRate.Dock = System.Windows.Forms.DockStyle.Fill;
      this.btnCalRate.Location = new System.Drawing.Point(5, 5);
      this.btnCalRate.Name = "btnCalRate";
      this.btnCalRate.Padding = new System.Windows.Forms.Padding(5);
      this.btnCalRate.Size = new System.Drawing.Size(314, 49);
      this.btnCalRate.TabIndex = 2;
      this.btnCalRate.Text = "Calculate Interest Rate";
      this.btnCalRate.UseVisualStyleBackColor = true;
      this.btnCalRate.Click += new System.EventHandler(this.btnCalRate_Click_1);
      // 
      // FormMain
      // 
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
      this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
      this.ClientSize = new System.Drawing.Size(384, 183);
      this.Controls.Add(this.splitContainer1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.MaximizeBox = false;
      this.Name = "FormMain";
      this.Padding = new System.Windows.Forms.Padding(30);
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Main";
      this.splitContainer1.Panel1.ResumeLayout(false);
      this.splitContainer1.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
      this.splitContainer1.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private global::System.Windows.Forms.SplitContainer splitContainer1;
    private global::System.Windows.Forms.Button btnAdminForm;
    private System.Windows.Forms.Button btnCalRate;
  }
}