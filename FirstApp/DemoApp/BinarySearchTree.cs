﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoApp
{

  public class BinarySearchTree<T>
  {
    public delegate int Comparer(T t1, T t2);

    Node root;
    Comparer comparer;
    public BinarySearchTree(Comparer mComparer)
    {
      comparer = mComparer;
    }

    public void Add(T value)
    {
      root = AddRec(root, value);
    }

    private Node AddRec(Node cur, T val)
    {
      if (cur == null) return new Node(val);
      if (comparer(cur.value, val) > 0) cur.left = AddRec(cur.left, val);
      else if (comparer(cur.value, val) < 0) cur.right = AddRec(cur.right, val);
      return cur;
    }

    public List<T> toList()
    {
      List<T> result = new List<T>();
      InOrderTravel(root, ref result);
      return result;
    }

    private void InOrderTravel(Node root, ref List<T> res)
    {
      if (root == null) return;
      InOrderTravel(root.left, ref res);
      res.Add(root.value);
      InOrderTravel(root.right, ref res);
    }

    public void Remove(T val)
    {
      root = RemoveRec(root, val);
    }

    private Node RemoveRec(Node cur, T val)
    {
      if (cur == null) return cur;
      if (comparer(cur.value, val) > 0) cur.left = RemoveRec(cur.left, val);
      else if (comparer(cur.value, val) < 0) cur.right = RemoveRec(cur.right, val);
      else
      {
        if (cur.left == null) return cur.right;
        if (cur.right == null) return cur.left;
        Node min = findMin(cur.right);
        cur.value = min.value;
        RemoveRec(cur.right, min.value);
      }
      return cur;
    }
    public T find(T val)
    {
      return findRec(root, val);
    }

    private T findRec(Node cur, T val)
    {
      if (cur == null) return default(T);
      if (comparer(cur.value, val) > 0) return findRec(cur.left, val);
      if (comparer(cur.value, val) < 0) return findRec(cur.right, val);
      return cur.value;
    }
    public T findLower(T val, Comparer deepComparer)
    {
      T result = default(T);
      if (deepComparer == null) deepComparer = comparer;
      findLowerRec(root, val, ref result, deepComparer);
      return result;
    }

    private void findLowerRec(Node cur, T val, ref T tmp, Comparer dComperer)
    {
      if (cur == null) return;
      if (comparer(cur.value, val) > 0) findLowerRec(cur.left, val, ref tmp, dComperer);
      else if (comparer(cur.value, val) <= 0)
      {
        if (dComperer(cur.value, val) == 0) tmp = cur.value;
        findLowerRec(cur.right, val, ref tmp, dComperer);
      }
    }

    private Node findMin(Node min)
    {
      while (min.left != null)
      {
        min = min.left;
      }
      return min;
    }

    public void findAndReplace(T val)
    {
      findAndReplaceRec(root, val);
    }

    private void findAndReplaceRec(Node cur, T val)
    {
      if (cur == null) return;
      if (comparer(cur.value, val) > 0) findAndReplaceRec(cur.left, val);
      else if (comparer(cur.value, val) < 0) findAndReplaceRec(cur.right, val);
      else cur.value = val;
    }

    class Node
    {
      public Node(T val)
      {
        value = val;
      }
      public T value { get; set; }
      internal Node left, right;
    }
  }
}
