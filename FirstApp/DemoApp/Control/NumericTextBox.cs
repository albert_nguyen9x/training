﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoApp
{
  public partial class NumericTextBox : TextBox
  {
    public bool EnableInputDot { get; set; } = true;
    public NumericTextBox()
    {
      InitializeComponent();
      TextChanged += NumericTextBox_TextChanged;
      PlaceholderText = "Input number in dollar($)";
    }

    private void NumericTextBox_TextChanged(object sender, EventArgs e)
    {
      StringBuilder textBuilder = new StringBuilder();
      int numDot = 0;
      int selection = SelectionStart;
      foreach (char c in Text)
      {
        if (EnableInputDot && c == '.')
        {
          if (numDot == 0)
          {
            textBuilder.Append(c);
            numDot++;
          }
          else selection--;
        }
        else if (c >= '0' && c <= '9')
        {
          textBuilder.Append(c);
        }
        else selection--;
      }
      Text = textBuilder.ToString();
      SelectionStart = selection;
    }
  }
}
