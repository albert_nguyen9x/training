﻿using DemoApp.Model;
using InterestRateSystem;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoApp
{
  public partial class FormUser : Form
  {
    public FormUser()
    {
      InitializeComponent();
      loadCombobox();
    }

    private void loadCombobox()
    {
      List<MyRate> data = MyInterestSystem.Instance().Rates;
      cboType.DataSource = data;
      cboType.SelectedItem = 0;
      cboType.DisplayMember = "Month";
      cboType.ValueMember = "ID";
    }

    private void mnSwitchAd_Click(object sender, EventArgs e)
    {
      this.Hide();
      FormAdmin formAdmin = new FormAdmin();
      formAdmin.ShowDialog();
      Close();
    }

    private void btnCreate_Click(object sender, EventArgs e)
    {
      if (string.IsNullOrWhiteSpace(txtName.Text) || string.IsNullOrWhiteSpace(txtAmount.Text))
      {
        MessageBox.Show("Please fill full data", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }
      float amount;
      if(!float.TryParse(txtAmount.Text, out amount))
      {
        MessageBox.Show("Wrong input amount, this is only number [0-9]", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }
      //string type = ((string)cboType.SelectedItem).Split("(")[0];
      /*InterestRateSystem.InterestRateSystem.Instance().Add(new Account
      {
        Name = txtName.Text,
        Amount = amount,
        Month = int.Parse(cboType.SelectedValue.ToString())
      }) ;*/
      /*int month = int.Parse(cboType.SelectedValue.ToString());
      float interestRate = InterestRateCal.CalculatorInterestRate(InterestRateRepository.findInterestRate(month), amount);
      float bonusRate = InterestRateCal.CalculatorBonusRateByRateIdAndAmount(InterestRateRepository.findBonusRateByRateIdAndAmount(month, amount), amount);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.AppendLine($"Amount:{amount}$");
      stringBuilder.AppendLine($"Interest Rate Amount: {interestRate}$");
      if (bonusRate != 0f) stringBuilder.AppendLine($"Bonus Amount: {bonusRate}$");
      stringBuilder.AppendLine($"Total: {amount + interestRate + bonusRate}$");
      MessageBox.Show($"Success create account for {txtName.Text}\n{stringBuilder.ToString()}", "Create successfull", MessageBoxButtons.OK, MessageBoxIcon.Information);*/
      txtAmount.Text = "";
      txtName.Text = "";
      cboType.SelectedIndex = 0;
    }

    private void btnSearch_Click(object sender, EventArgs e)
    {
      if (string.IsNullOrWhiteSpace(txtSearch.Text))
      {
        MessageBox.Show("Please enter name data", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        txtSearch.Focus();
        return;
      }
      //List<Account> findData = InterestRateSystem.Instance().searchAccount(txtSearch.Text);
      txtSearch.Text = "";
      //List<Rate> rates = InterestRateSystem.Instance().Rates;
      //var datas = (from rAccount in findData
      //             join rRate in rates on rAccount.Month equals rRate.ID
      //             select new { Name = rAccount.Name, Amount = rAccount.Amount, Type = Helper.intMonthToString(rRate.Month), Rate = $"{rRate.InterestRate * 100}%" }).ToList();
      //lstAccount.Items.Clear();
      //List<ListViewItem> items = new List<ListViewItem>();
      //datas.ForEach((a) =>
      //{
      //  items.Add(new ListViewItem(new string[] { a.Name, $"{a.Amount}$", a.Type, a.Rate }));
      //});
      //lstAccount.Items.AddRange(items.ToArray());
    }
  }
}
