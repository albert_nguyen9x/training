﻿using DemoApp.Model;
using InterestRateSystem;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoApp
{
  public partial class CreateRecord : Form
  {
    public enum FormType
    {
      RateType,
      BonusType
    }
    public bool IsCreateForm { get; set; } = true;
    object mData;
    public FormType FType { get; set; } = FormType.BonusType;
    public CreateRecord(object data, bool createForm = true, FormType formType = FormType.BonusType)
    {
      InitializeComponent();
      IsCreateForm = createForm;
      FType = formType;
      if (FType == FormType.RateType)
      {
        groupBox2.Controls.Remove(panel3);
        Height -= panel3.Height;
      }
      if (!IsCreateForm)
      {
        string text = "Update ";
        mData = data;


        switch (FType)
        {
          case FormType.RateType:
            text += "Rate";
            MyRate rate = data as MyRate;
            txtMonth.Text = rate.Month + "";
            txtRate.Text = rate.InterestRate*100 + "";
            break;
          case FormType.BonusType:
            text += "Bonus";
            MyBonus bonus = data as MyBonus;
            txtAmount.ReadOnly = true;
            txtMonth.Text = bonus.Month + "";
            txtAmount.Text = bonus.Amount + "";
            txtRate.Text = bonus.BonusRate*100 + "";
            break;
        }
        this.Text = text;
        groupBox2.Text = text;
        btnCreate.Text = "Update";
        txtMonth.ReadOnly = true;
        txtRate.Select();
        txtRate.Focus();
      }
      else
      {
        string text = "Create ";
        switch (FType)
        {
          case FormType.RateType:
            text += "Rate";
            txtMonth.Select();
            txtMonth.Focus();
            break;
          case FormType.BonusType:
            text += "Bonus";
            break;
        }
        this.Text = text;
        groupBox2.Text = text;
      }
    }

    private void btnCreate_Click(object sender, EventArgs e)
    {
      if (FType == FormType.RateType)
      {
        if (string.IsNullOrWhiteSpace(txtMonth.Text) || string.IsNullOrWhiteSpace(txtRate.Text))
        {
          MessageBox.Show("Please fill full data", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
          return;
        }
        if (IsCreateForm)
        {
          if (MyInterestSystem.Instance().findRateByMonth(int.Parse(txtMonth.Text)) != null)
          {
            MessageBox.Show($"This rate for {txtMonth.Text} month exist!!!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
          }
          MyRate add = new MyRate()
          {
            Month = int.Parse(txtMonth.Text),
            InterestRate = float.Parse(txtRate.Text) / 100
          };
          MyInterestSystem.Instance().Rates.Add(add);
        }
        else
        {
          MyRate change = mData as MyRate;
          MyInterestSystem.Instance().Rates.Remove(change);
          change.InterestRate = float.Parse(txtRate.Text)/100f;
          MyInterestSystem.Instance().Rates.Add(change);
        }

      }
      else
      {
        if (string.IsNullOrWhiteSpace(txtAmount.Text) || string.IsNullOrWhiteSpace(txtRate.Text) || string.IsNullOrWhiteSpace(txtMonth.Text))
        {
          MessageBox.Show("Please fill full data", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
          return;
        }
        if (IsCreateForm)
        {
          if (MyInterestSystem.Instance().findBonusByAmountAndMonth(float.Parse(txtAmount.Text), int.Parse(txtMonth.Text)) != null)
          {
            MessageBox.Show($"This bonus for >{txtAmount.Text}$ and {txtMonth.Text} months  exist!!!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
          }
          MyBonus add = new MyBonus()
          {
            Amount = float.Parse(txtAmount.Text),
            BonusRate = float.Parse(txtRate.Text) / 100,
            Month = int.Parse(txtMonth.Text)
          };
          MyInterestSystem.Instance().Bonus.Add(add);
        }
        else
        {
          MyBonus change = mData as MyBonus;
          change.BonusRate = float.Parse(txtRate.Text)/100f;
          MyInterestSystem.Instance().Bonus.findAndReplace(change);
        }
      }
      DialogResult = DialogResult.OK;
      Close();
    }
  }
}
