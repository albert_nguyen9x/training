﻿
namespace DemoApp
{
  partial class CreateRecord
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.btnCreate = new System.Windows.Forms.Button();
      this.panel2 = new System.Windows.Forms.Panel();
      this.txtRate = new DemoApp.NumericTextBox();
      this.label3 = new System.Windows.Forms.Label();
      this.panel4 = new System.Windows.Forms.Panel();
      this.txtMonth = new DemoApp.NumericTextBox();
      this.label5 = new System.Windows.Forms.Label();
      this.panel3 = new System.Windows.Forms.Panel();
      this.txtAmount = new DemoApp.NumericTextBox();
      this.label4 = new System.Windows.Forms.Label();
      this.groupBox2.SuspendLayout();
      this.panel2.SuspendLayout();
      this.panel4.SuspendLayout();
      this.panel3.SuspendLayout();
      this.SuspendLayout();
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.btnCreate);
      this.groupBox2.Controls.Add(this.panel2);
      this.groupBox2.Controls.Add(this.panel4);
      this.groupBox2.Controls.Add(this.panel3);
      this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox2.Location = new System.Drawing.Point(0, 0);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Padding = new System.Windows.Forms.Padding(5);
      this.groupBox2.Size = new System.Drawing.Size(293, 203);
      this.groupBox2.TabIndex = 2;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "Create record";
      // 
      // btnCreate
      // 
      this.btnCreate.Dock = System.Windows.Forms.DockStyle.Top;
      this.btnCreate.Location = new System.Drawing.Point(5, 164);
      this.btnCreate.Name = "btnCreate";
      this.btnCreate.Size = new System.Drawing.Size(283, 28);
      this.btnCreate.TabIndex = 4;
      this.btnCreate.Text = "Create";
      this.btnCreate.UseVisualStyleBackColor = true;
      this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
      // 
      // panel2
      // 
      this.panel2.Controls.Add(this.txtRate);
      this.panel2.Controls.Add(this.label3);
      this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel2.Location = new System.Drawing.Point(5, 109);
      this.panel2.Name = "panel2";
      this.panel2.Padding = new System.Windows.Forms.Padding(3);
      this.panel2.Size = new System.Drawing.Size(283, 55);
      this.panel2.TabIndex = 1;
      // 
      // txtRate
      // 
      this.txtRate.Dock = System.Windows.Forms.DockStyle.Fill;
      this.txtRate.EnableInputDot = true;
      this.txtRate.Location = new System.Drawing.Point(3, 18);
      this.txtRate.Name = "txtRate";
      this.txtRate.PlaceholderText = "200 => 200%";
      this.txtRate.Size = new System.Drawing.Size(277, 23);
      this.txtRate.TabIndex = 3;
      // 
      // label3
      // 
      this.label3.Dock = System.Windows.Forms.DockStyle.Top;
      this.label3.Location = new System.Drawing.Point(3, 3);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(277, 15);
      this.label3.TabIndex = 0;
      this.label3.Text = "Rate(%)";
      this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // panel4
      // 
      this.panel4.Controls.Add(this.txtMonth);
      this.panel4.Controls.Add(this.label5);
      this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel4.Location = new System.Drawing.Point(5, 65);
      this.panel4.Name = "panel4";
      this.panel4.Padding = new System.Windows.Forms.Padding(3);
      this.panel4.Size = new System.Drawing.Size(283, 44);
      this.panel4.TabIndex = 6;
      // 
      // txtMonth
      // 
      this.txtMonth.Dock = System.Windows.Forms.DockStyle.Fill;
      this.txtMonth.EnableInputDot = false;
      this.txtMonth.Location = new System.Drawing.Point(3, 18);
      this.txtMonth.Name = "txtMonth";
      this.txtMonth.PlaceholderText = "Input months";
      this.txtMonth.Size = new System.Drawing.Size(277, 23);
      this.txtMonth.TabIndex = 2;
      // 
      // label5
      // 
      this.label5.Dock = System.Windows.Forms.DockStyle.Top;
      this.label5.Location = new System.Drawing.Point(3, 3);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(277, 15);
      this.label5.TabIndex = 4;
      this.label5.Text = "Month";
      this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // panel3
      // 
      this.panel3.Controls.Add(this.txtAmount);
      this.panel3.Controls.Add(this.label4);
      this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel3.Location = new System.Drawing.Point(5, 21);
      this.panel3.Name = "panel3";
      this.panel3.Padding = new System.Windows.Forms.Padding(3);
      this.panel3.Size = new System.Drawing.Size(283, 44);
      this.panel3.TabIndex = 0;
      // 
      // txtAmount
      // 
      this.txtAmount.Dock = System.Windows.Forms.DockStyle.Fill;
      this.txtAmount.EnableInputDot = true;
      this.txtAmount.Location = new System.Drawing.Point(3, 18);
      this.txtAmount.Name = "txtAmount";
      this.txtAmount.PlaceholderText = "Input amount($)";
      this.txtAmount.Size = new System.Drawing.Size(277, 23);
      this.txtAmount.TabIndex = 1;
      // 
      // label4
      // 
      this.label4.Dock = System.Windows.Forms.DockStyle.Top;
      this.label4.Location = new System.Drawing.Point(3, 3);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(277, 15);
      this.label4.TabIndex = 0;
      this.label4.Text = "Amount";
      this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // CreateRecord
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(293, 203);
      this.Controls.Add(this.groupBox2);
      this.Name = "CreateRecord";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "CreateRecord";
      this.groupBox2.ResumeLayout(false);
      this.panel2.ResumeLayout(false);
      this.panel2.PerformLayout();
      this.panel4.ResumeLayout(false);
      this.panel4.PerformLayout();
      this.panel3.ResumeLayout(false);
      this.panel3.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.Button btnCreate;
    private System.Windows.Forms.Panel panel2;
    private NumericTextBox txtRate;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Panel panel4;
    private NumericTextBox txtMonth;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Panel panel3;
    private NumericTextBox txtAmount;
    private System.Windows.Forms.Label label4;
  }
}