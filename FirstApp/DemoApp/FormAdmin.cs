﻿using DemoApp.Model;
using InterestRateSystem;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoApp
{
  public partial class FormAdmin : Form
  {
    public FormAdmin()
    {
      InitializeComponent();
      loadAccount();
      loadRate();
      loadBonus();
      tabAdmin.TabPages.Remove(tabAccount);
    }

    private void loadBonus()
    {
      lstBonus.DataSource = null;
      List<MyBonus> bonus = MyInterestSystem.Instance().Bonus.toList();
      lstBonus.DataSource = bonus;
      lstBonus.Columns[0].ReadOnly = true;
      lstBonus.Columns[1].ReadOnly = true;
    }

    private void loadRate()
    {
      lstRate.DataSource = null;
      List<MyRate> rates = MyInterestSystem.Instance().Rates.OrderBy(rate => rate.Month).ToList();
      lstRate.DataSource = rates;
      lstRate.Columns[0].ReadOnly = true;
    }

    private void loadAccount()
    {
      /*List<Account> accounts = InterestRateSystem.InterestRateSystem.Instance().Accounts;
      BinarySearchTree<Rate> rates = InterestRateSystem.InterestRateSystem.Instance().Rates;
      var datas = (from rAccount in accounts
                   select new { Name = rAccount.Name, Amount = rAccount.Amount, Type = Helper.intMonthToString(rAccount.Month) }).ToList();
      lstAccount.Items.Clear();
      List<ListViewItem> items = new List<ListViewItem>();
      datas.ForEach((a) =>
      {
        items.Add(new ListViewItem(new string[] { a.Name, $"{a.Amount}$", a.Type}));
      });
      lstAccount.Items.AddRange(items.ToArray());*/

    }

    private void mnuSwitch_Click(object sender, EventArgs e)
    {
      this.Hide();
      FormUser formUser = new FormUser();
      formUser.ShowDialog();
      Close();
    }
    private void btnCreateRate_Click(object sender, EventArgs e)
    {
      CreateRecord form = new CreateRecord(null, formType: CreateRecord.FormType.RateType);
      DialogResult result = form.ShowDialog();
      if(result== DialogResult.OK) loadRate();
    }

    private void btnCreateBonus_Click(object sender, EventArgs e)
    {
      CreateRecord form = new CreateRecord(null);
      DialogResult result = form.ShowDialog();
      if (result == DialogResult.OK) loadBonus();
    }

    private void lstRate_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
    {
      MyRate rate = lstRate.Rows[e.RowIndex].DataBoundItem as MyRate;
      CreateRecord form = new CreateRecord(rate, false, CreateRecord.FormType.RateType);
      DialogResult result = form.ShowDialog();
      if (result == DialogResult.OK) loadRate();
    }

    private void lstBonus_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
    {
      MyBonus bonus = lstBonus.Rows[e.RowIndex].DataBoundItem as MyBonus;
      CreateRecord form = new CreateRecord(bonus, false);
      DialogResult result = form.ShowDialog();
      if (result == DialogResult.OK) loadBonus();
    }

    private void lstRate_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
    {
      if(e.ColumnIndex == 1)
      {
        e.CellStyle.Format = "P2";
      }
    }

    private void lstBonus_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
    {
      if (e.ColumnIndex == 2)
      {
        e.CellStyle.Format = "P2";
      }
    }
  }
}
