﻿using DemoApp.Model;
using InterestRateSystem.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoApp
{
  internal class MyCustomerInfoProvider : ICustomerProvider
  {
    public IBonus getRateBonusBigAmount(int month, float amount)
    {
      return InterestRateRepository.findBonusRateByRateIdAndAmount(month, amount);
    }

    public IRate getRateByMonth(int month)
    {
      return InterestRateRepository.findInterestRate(month);
    }
  }
  internal class MyInterestSystem
  {
    static MyInterestSystem instance = new MyInterestSystem();
    public List<MyRate> Rates { get; set; }
    public BinarySearchTree<MyBonus> Bonus { get; set; }
    private MyInterestSystem()
    {

      //Rates = new BinarySearchTree<MyRate>((a, b) => a.Month - b.Month);
      Rates = new List<MyRate>();
      /*Accounts = new List<Account>()
      {
        new Account
        {
          Name = "Awesome",
          Amount = 2000,
          Month = 1
        },
        new Account
        {
          Name = "Arrow",
          Amount = 800,
          Month = 2
        },
        new Account
        {
          Name = "Brushia",
          Amount = 5000,
          Month = 6
        },
        new Account
        {
          Name = "Cavani",
          Amount = 100,
          Month = 12
        },
        new Account
        {
          Name = "Davison",
          Amount = 3000,
          Month = 18
        },
        new Account
        {
          Name = "Edison",
          Amount = 6000,
          Month = 6
        }
      };*/
      Bonus = new BinarySearchTree<MyBonus>((a, b) => {
        float dif = a.Amount - b.Amount;
        if (dif > 0f) return 1;
        else if (dif < 0f) return -1;
        return a.Month - b.Month;
      });
      intDb();
    }

    private void intDb()
    {
      Bonus.Add(new MyBonus()
      {
        Amount = 3000,
        BonusRate = 0.015f,
        Month = 6
      });
      Bonus.Add(new MyBonus()
      {
        Amount = 500,
        BonusRate = 0.005f,
        Month = 12
      });
      Bonus.Add(new MyBonus()
      {
        Amount = 1000,
        BonusRate = 0.015f,
        Month = 12
      });
      Bonus.Add(new MyBonus()
      {
        Amount = 1000,
        BonusRate = 0.01f,
        Month = 6
      });
      Rates.Add(new MyRate()
      {
        Month = 1,
        InterestRate = 0.02f
      });
      Rates.Add(new MyRate()
      {
        Month = 2,
        InterestRate = 0.03f
      });
      Rates.Add(new MyRate()
      {
        Month = 6,
        InterestRate = 0.05f
      });
      Rates.Add(new MyRate()
      {
        Month = 12,
        InterestRate = 0.08f
      });
      Rates.Add(new MyRate()
      {
        Month = 18,
        InterestRate = 0.085f
      });
    }

    public static MyInterestSystem Instance() => instance;
    /*public void Add(Account a)
    {
      Accounts.Add(a);
    }
    public List<Account> searchAccount(string name)
    {
      return Accounts.Where(acc => acc.Name == name).ToList();
    }*/

    public MyRate findRateByMonth(int months)
    {
      MyRate rate = Rates.Where((r) => r.Month == months).FirstOrDefault();
      return rate;

    }

    public MyBonus findBonusByAmountAndMonth(float amount, int month)
    {
      MyBonus bonus = Bonus.toList().Where((r) => r.Amount == amount && r.Month == month).FirstOrDefault();
      return bonus;
    }
  }
}
