﻿using InterestRateSystem;
using InterestRateSystem.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoApp.Model
{
  internal class MyRate:IRate
  {
    public int Month { get; set; }
    public float InterestRate { get; set; }

    public float Rating()
    {
      return InterestRate;
    }
  }
}
