﻿using InterestRateSystem.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoApp.Model
{
  public enum CustomerType
  {
    Personal, Company, CoperativeBank
  }
  internal abstract class MyCustomer : ICustomer
  {
    public abstract float Rating();
    

    internal class CompanyCustomer :MyCustomer
    {
      public override float Rating()
      {
        return 1;
      }
    }
    internal class CoperativeBankCustomer : MyCustomer
    {
      public override float Rating()
      {
        return 2;
      }
    }
    internal class PersonalCustomer : MyCustomer
    {
      public override float Rating()
      {
        return 0;
      }
    }
  }
}
