﻿
namespace DemoApp
{
  partial class FormAdmin
  {
    /// <summary>
    ///  Required designer variable.
    /// </summary>
    private global::System.ComponentModel.IContainer components = null;

    /// <summary>
    ///  Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    ///  Required method for Designer support - do not modify
    ///  the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      this.mnAdmin = new System.Windows.Forms.MenuStrip();
      this.mnuSwitch = new System.Windows.Forms.ToolStripMenuItem();
      this.tabAdmin = new System.Windows.Forms.TabControl();
      this.tabAccount = new System.Windows.Forms.TabPage();
      this.lstAccount = new System.Windows.Forms.ListView();
      this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
      this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
      this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
      this.tabRate = new System.Windows.Forms.TabPage();
      this.splitContainer1 = new System.Windows.Forms.SplitContainer();
      this.btnDeleteRate = new System.Windows.Forms.Button();
      this.splitter2 = new System.Windows.Forms.Splitter();
      this.btnCreateRate = new System.Windows.Forms.Button();
      this.lstRate = new System.Windows.Forms.DataGridView();
      this.tabBonus = new System.Windows.Forms.TabPage();
      this.splitContainer2 = new System.Windows.Forms.SplitContainer();
      this.btnDeleteBonus = new System.Windows.Forms.Button();
      this.splitter1 = new System.Windows.Forms.Splitter();
      this.btnCreateBonus = new System.Windows.Forms.Button();
      this.lstBonus = new System.Windows.Forms.DataGridView();
      this.mnAdmin.SuspendLayout();
      this.tabAdmin.SuspendLayout();
      this.tabAccount.SuspendLayout();
      this.tabRate.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
      this.splitContainer1.Panel1.SuspendLayout();
      this.splitContainer1.Panel2.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.lstRate)).BeginInit();
      this.tabBonus.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
      this.splitContainer2.Panel1.SuspendLayout();
      this.splitContainer2.Panel2.SuspendLayout();
      this.splitContainer2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.lstBonus)).BeginInit();
      this.SuspendLayout();
      // 
      // mnAdmin
      // 
      this.mnAdmin.ImageScalingSize = new System.Drawing.Size(20, 20);
      this.mnAdmin.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuSwitch});
      this.mnAdmin.Location = new System.Drawing.Point(0, 0);
      this.mnAdmin.Name = "mnAdmin";
      this.mnAdmin.Size = new System.Drawing.Size(584, 24);
      this.mnAdmin.TabIndex = 0;
      this.mnAdmin.Text = "menuStrip1";
      // 
      // mnuSwitch
      // 
      this.mnuSwitch.Name = "mnuSwitch";
      this.mnuSwitch.Size = new System.Drawing.Size(80, 20);
      this.mnuSwitch.Text = "Switch User";
      // 
      // tabAdmin
      // 
      this.tabAdmin.Controls.Add(this.tabAccount);
      this.tabAdmin.Controls.Add(this.tabRate);
      this.tabAdmin.Controls.Add(this.tabBonus);
      this.tabAdmin.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tabAdmin.Location = new System.Drawing.Point(0, 24);
      this.tabAdmin.Name = "tabAdmin";
      this.tabAdmin.SelectedIndex = 0;
      this.tabAdmin.Size = new System.Drawing.Size(584, 387);
      this.tabAdmin.TabIndex = 1;
      // 
      // tabAccount
      // 
      this.tabAccount.Controls.Add(this.lstAccount);
      this.tabAccount.Location = new System.Drawing.Point(4, 24);
      this.tabAccount.Name = "tabAccount";
      this.tabAccount.Padding = new System.Windows.Forms.Padding(3);
      this.tabAccount.Size = new System.Drawing.Size(576, 359);
      this.tabAccount.TabIndex = 0;
      this.tabAccount.Text = "Account";
      this.tabAccount.UseVisualStyleBackColor = true;
      // 
      // lstAccount
      // 
      this.lstAccount.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
      this.lstAccount.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lstAccount.GridLines = true;
      this.lstAccount.HideSelection = false;
      this.lstAccount.Location = new System.Drawing.Point(3, 3);
      this.lstAccount.Name = "lstAccount";
      this.lstAccount.Size = new System.Drawing.Size(570, 353);
      this.lstAccount.TabIndex = 0;
      this.lstAccount.UseCompatibleStateImageBehavior = false;
      this.lstAccount.View = System.Windows.Forms.View.Details;
      // 
      // columnHeader1
      // 
      this.columnHeader1.Text = "Name";
      this.columnHeader1.Width = 200;
      // 
      // columnHeader2
      // 
      this.columnHeader2.Text = "Amount";
      this.columnHeader2.Width = 100;
      // 
      // columnHeader3
      // 
      this.columnHeader3.Text = "Type";
      this.columnHeader3.Width = 100;
      // 
      // tabRate
      // 
      this.tabRate.Controls.Add(this.splitContainer1);
      this.tabRate.Location = new System.Drawing.Point(4, 24);
      this.tabRate.Name = "tabRate";
      this.tabRate.Padding = new System.Windows.Forms.Padding(3);
      this.tabRate.Size = new System.Drawing.Size(576, 359);
      this.tabRate.TabIndex = 1;
      this.tabRate.Text = "Rate";
      this.tabRate.UseVisualStyleBackColor = true;
      // 
      // splitContainer1
      // 
      this.splitContainer1.Cursor = System.Windows.Forms.Cursors.Default;
      this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer1.Location = new System.Drawing.Point(3, 3);
      this.splitContainer1.Name = "splitContainer1";
      this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer1.Panel1
      // 
      this.splitContainer1.Panel1.Controls.Add(this.btnDeleteRate);
      this.splitContainer1.Panel1.Controls.Add(this.splitter2);
      this.splitContainer1.Panel1.Controls.Add(this.btnCreateRate);
      this.splitContainer1.Panel1.Padding = new System.Windows.Forms.Padding(3);
      // 
      // splitContainer1.Panel2
      // 
      this.splitContainer1.Panel2.Controls.Add(this.lstRate);
      this.splitContainer1.Size = new System.Drawing.Size(570, 353);
      this.splitContainer1.SplitterDistance = 34;
      this.splitContainer1.TabIndex = 1;
      // 
      // btnDeleteRate
      // 
      this.btnDeleteRate.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnDeleteRate.Location = new System.Drawing.Point(109, 3);
      this.btnDeleteRate.Name = "btnDeleteRate";
      this.btnDeleteRate.Size = new System.Drawing.Size(103, 28);
      this.btnDeleteRate.TabIndex = 8;
      this.btnDeleteRate.Text = "Delete";
      this.btnDeleteRate.UseVisualStyleBackColor = true;
      this.btnDeleteRate.Visible = false;
      // 
      // splitter2
      // 
      this.splitter2.Location = new System.Drawing.Point(106, 3);
      this.splitter2.Name = "splitter2";
      this.splitter2.Size = new System.Drawing.Size(3, 28);
      this.splitter2.TabIndex = 7;
      this.splitter2.TabStop = false;
      this.splitter2.Visible = false;
      // 
      // btnCreateRate
      // 
      this.btnCreateRate.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnCreateRate.Location = new System.Drawing.Point(3, 3);
      this.btnCreateRate.Name = "btnCreateRate";
      this.btnCreateRate.Size = new System.Drawing.Size(103, 28);
      this.btnCreateRate.TabIndex = 6;
      this.btnCreateRate.Text = "Create";
      this.btnCreateRate.UseVisualStyleBackColor = true;
      this.btnCreateRate.Click += new System.EventHandler(this.btnCreateRate_Click);
      // 
      // lstRate
      // 
      this.lstRate.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
      this.lstRate.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
      this.lstRate.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.lstRate.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lstRate.GridColor = System.Drawing.SystemColors.Control;
      this.lstRate.Location = new System.Drawing.Point(0, 0);
      this.lstRate.Name = "lstRate";
      this.lstRate.ReadOnly = true;
      this.lstRate.RowHeadersWidth = 51;
      this.lstRate.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
      dataGridViewCellStyle1.NullValue = null;
      this.lstRate.RowsDefaultCellStyle = dataGridViewCellStyle1;
      this.lstRate.RowTemplate.Height = 25;
      this.lstRate.Size = new System.Drawing.Size(570, 315);
      this.lstRate.TabIndex = 0;
      this.lstRate.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.lstRate_CellDoubleClick);
      this.lstRate.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.lstRate_CellFormatting);
      // 
      // tabBonus
      // 
      this.tabBonus.Controls.Add(this.splitContainer2);
      this.tabBonus.Location = new System.Drawing.Point(4, 24);
      this.tabBonus.Name = "tabBonus";
      this.tabBonus.Size = new System.Drawing.Size(576, 359);
      this.tabBonus.TabIndex = 2;
      this.tabBonus.Text = "Bonus";
      this.tabBonus.UseVisualStyleBackColor = true;
      // 
      // splitContainer2
      // 
      this.splitContainer2.Cursor = System.Windows.Forms.Cursors.Default;
      this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer2.Location = new System.Drawing.Point(0, 0);
      this.splitContainer2.Name = "splitContainer2";
      this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer2.Panel1
      // 
      this.splitContainer2.Panel1.Controls.Add(this.btnDeleteBonus);
      this.splitContainer2.Panel1.Controls.Add(this.splitter1);
      this.splitContainer2.Panel1.Controls.Add(this.btnCreateBonus);
      this.splitContainer2.Panel1.Padding = new System.Windows.Forms.Padding(3);
      // 
      // splitContainer2.Panel2
      // 
      this.splitContainer2.Panel2.Controls.Add(this.lstBonus);
      this.splitContainer2.Size = new System.Drawing.Size(576, 359);
      this.splitContainer2.SplitterDistance = 34;
      this.splitContainer2.TabIndex = 2;
      // 
      // btnDeleteBonus
      // 
      this.btnDeleteBonus.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnDeleteBonus.Location = new System.Drawing.Point(109, 3);
      this.btnDeleteBonus.Name = "btnDeleteBonus";
      this.btnDeleteBonus.Size = new System.Drawing.Size(103, 28);
      this.btnDeleteBonus.TabIndex = 7;
      this.btnDeleteBonus.Text = "Delete";
      this.btnDeleteBonus.UseVisualStyleBackColor = true;
      this.btnDeleteBonus.Visible = false;
      // 
      // splitter1
      // 
      this.splitter1.Location = new System.Drawing.Point(106, 3);
      this.splitter1.Name = "splitter1";
      this.splitter1.Size = new System.Drawing.Size(3, 28);
      this.splitter1.TabIndex = 6;
      this.splitter1.TabStop = false;
      this.splitter1.Visible = false;
      // 
      // btnCreateBonus
      // 
      this.btnCreateBonus.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnCreateBonus.Location = new System.Drawing.Point(3, 3);
      this.btnCreateBonus.Name = "btnCreateBonus";
      this.btnCreateBonus.Size = new System.Drawing.Size(103, 28);
      this.btnCreateBonus.TabIndex = 5;
      this.btnCreateBonus.Text = "Create";
      this.btnCreateBonus.UseVisualStyleBackColor = true;
      this.btnCreateBonus.Click += new System.EventHandler(this.btnCreateBonus_Click);
      // 
      // lstBonus
      // 
      this.lstBonus.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
      this.lstBonus.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
      this.lstBonus.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.lstBonus.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lstBonus.GridColor = System.Drawing.SystemColors.Control;
      this.lstBonus.Location = new System.Drawing.Point(0, 0);
      this.lstBonus.Name = "lstBonus";
      this.lstBonus.ReadOnly = true;
      this.lstBonus.RowHeadersWidth = 51;
      this.lstBonus.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
      this.lstBonus.RowTemplate.Height = 25;
      this.lstBonus.Size = new System.Drawing.Size(576, 321);
      this.lstBonus.TabIndex = 0;
      this.lstBonus.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.lstBonus_CellDoubleClick);
      this.lstBonus.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.lstBonus_CellFormatting);
      // 
      // FormAdmin
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(584, 411);
      this.Controls.Add(this.tabAdmin);
      this.Controls.Add(this.mnAdmin);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.MainMenuStrip = this.mnAdmin;
      this.MaximizeBox = false;
      this.Name = "FormAdmin";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Admin";
      this.mnAdmin.ResumeLayout(false);
      this.mnAdmin.PerformLayout();
      this.tabAdmin.ResumeLayout(false);
      this.tabAccount.ResumeLayout(false);
      this.tabRate.ResumeLayout(false);
      this.splitContainer1.Panel1.ResumeLayout(false);
      this.splitContainer1.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
      this.splitContainer1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.lstRate)).EndInit();
      this.tabBonus.ResumeLayout(false);
      this.splitContainer2.Panel1.ResumeLayout(false);
      this.splitContainer2.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
      this.splitContainer2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.lstBonus)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private global::System.Windows.Forms.MenuStrip mnAdmin;
    private global::System.Windows.Forms.ToolStripMenuItem mnuSwitch;
    private global::System.Windows.Forms.TabControl tabAdmin;
    private global::System.Windows.Forms.TabPage tabAccount;
    private global::System.Windows.Forms.ListView lstAccount;
    private global::System.Windows.Forms.TabPage tabRate;
    private global::System.Windows.Forms.ColumnHeader columnHeader1;
    private global::System.Windows.Forms.ColumnHeader columnHeader2;
    private global::System.Windows.Forms.ColumnHeader columnHeader3;
    private global::System.Windows.Forms.DataGridView lstRate;
    private System.Windows.Forms.SplitContainer splitContainer1;
    private System.Windows.Forms.TabPage tabBonus;
    private System.Windows.Forms.SplitContainer splitContainer2;
    private System.Windows.Forms.DataGridView lstBonus;
    private System.Windows.Forms.Splitter splitter1;
    private System.Windows.Forms.Button btnCreateBonus;
    private System.Windows.Forms.Button btnDeleteRate;
    private System.Windows.Forms.Splitter splitter2;
    private System.Windows.Forms.Button btnCreateRate;
    private System.Windows.Forms.Button btnDeleteBonus;
  }
}

