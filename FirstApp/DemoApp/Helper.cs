﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoApp
{
  public class Helper
  {
    public static string intMonthToString(int month)
    {
      int year = month / 12;
      if (year < 1)
      {
        if (month > 1)
          return $"{month} Months";
        else return $"{month} Month";
      }
      else if (year == 1)
      {
        if (month % 12 > 1)
          return $"1 Year {month % 12} Months";
        else if (month % 12 == 1) return $"1 Year 1 Month";
        else return $"1 Year";
      }
      else{
        if (month % 12 > 1)
          return $"{year} Years {month % 12} Months";
        else if (month % 12 == 1) return $"{year} Years 1 Month";
        else return $"{year} Years";
      }
    }
  }
}
