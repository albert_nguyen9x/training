﻿
namespace DemoApp
{
  partial class FormInterestRate
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private global::System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.splitContainer1 = new System.Windows.Forms.SplitContainer();
      this.splitContainer3 = new System.Windows.Forms.SplitContainer();
      this.panel2 = new System.Windows.Forms.Panel();
      this.txtAmount = new DemoApp.NumericTextBox();
      this.label2 = new System.Windows.Forms.Label();
      this.panel1 = new System.Windows.Forms.Panel();
      this.cboRType = new System.Windows.Forms.ComboBox();
      this.label1 = new System.Windows.Forms.Label();
      this.panel6 = new System.Windows.Forms.Panel();
      this.label3 = new System.Windows.Forms.Label();
      this.cboAccType = new System.Windows.Forms.ComboBox();
      this.panel7 = new System.Windows.Forms.Panel();
      this.txtRate = new System.Windows.Forms.Label();
      this.pnResult = new System.Windows.Forms.Panel();
      this.panel5 = new System.Windows.Forms.Panel();
      this.txtResult = new System.Windows.Forms.Label();
      this.panel4 = new System.Windows.Forms.Panel();
      this.label11 = new System.Windows.Forms.Label();
      this.panel3 = new System.Windows.Forms.Panel();
      this.btnCalculate = new System.Windows.Forms.Button();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
      this.splitContainer1.Panel1.SuspendLayout();
      this.splitContainer1.Panel2.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
      this.splitContainer3.Panel1.SuspendLayout();
      this.splitContainer3.Panel2.SuspendLayout();
      this.splitContainer3.SuspendLayout();
      this.panel2.SuspendLayout();
      this.panel1.SuspendLayout();
      this.panel6.SuspendLayout();
      this.panel7.SuspendLayout();
      this.pnResult.SuspendLayout();
      this.panel5.SuspendLayout();
      this.panel4.SuspendLayout();
      this.panel3.SuspendLayout();
      this.SuspendLayout();
      // 
      // splitContainer1
      // 
      this.splitContainer1.Cursor = System.Windows.Forms.Cursors.Default;
      this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer1.IsSplitterFixed = true;
      this.splitContainer1.Location = new System.Drawing.Point(10, 10);
      this.splitContainer1.Name = "splitContainer1";
      this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer1.Panel1
      // 
      this.splitContainer1.Panel1.Controls.Add(this.splitContainer3);
      this.splitContainer1.Panel1.Cursor = System.Windows.Forms.Cursors.Default;
      this.splitContainer1.Panel1.Padding = new System.Windows.Forms.Padding(15);
      // 
      // splitContainer1.Panel2
      // 
      this.splitContainer1.Panel2.Controls.Add(this.pnResult);
      this.splitContainer1.Panel2.Controls.Add(this.panel3);
      this.splitContainer1.Panel2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
      this.splitContainer1.Panel2.Padding = new System.Windows.Forms.Padding(2);
      this.splitContainer1.Size = new System.Drawing.Size(564, 296);
      this.splitContainer1.SplitterDistance = 124;
      this.splitContainer1.TabIndex = 0;
      // 
      // splitContainer3
      // 
      this.splitContainer3.Cursor = System.Windows.Forms.Cursors.Default;
      this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer3.Location = new System.Drawing.Point(15, 15);
      this.splitContainer3.Name = "splitContainer3";
      // 
      // splitContainer3.Panel1
      // 
      this.splitContainer3.Panel1.Controls.Add(this.panel2);
      this.splitContainer3.Panel1.Controls.Add(this.panel1);
      this.splitContainer3.Panel1.Padding = new System.Windows.Forms.Padding(2);
      // 
      // splitContainer3.Panel2
      // 
      this.splitContainer3.Panel2.Controls.Add(this.panel6);
      this.splitContainer3.Panel2.Controls.Add(this.panel7);
      this.splitContainer3.Panel2.Padding = new System.Windows.Forms.Padding(2);
      this.splitContainer3.Size = new System.Drawing.Size(534, 94);
      this.splitContainer3.SplitterDistance = 265;
      this.splitContainer3.TabIndex = 0;
      // 
      // panel2
      // 
      this.panel2.Controls.Add(this.txtAmount);
      this.panel2.Controls.Add(this.label2);
      this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel2.Location = new System.Drawing.Point(2, 47);
      this.panel2.Name = "panel2";
      this.panel2.Padding = new System.Windows.Forms.Padding(12);
      this.panel2.Size = new System.Drawing.Size(261, 45);
      this.panel2.TabIndex = 1;
      // 
      // txtAmount
      // 
      this.txtAmount.Dock = System.Windows.Forms.DockStyle.Right;
      this.txtAmount.EnableInputDot = true;
      this.txtAmount.Location = new System.Drawing.Point(117, 12);
      this.txtAmount.Name = "txtAmount";
      this.txtAmount.PlaceholderText = "Input number in dollar($)";
      this.txtAmount.Size = new System.Drawing.Size(132, 23);
      this.txtAmount.TabIndex = 7;
      this.txtAmount.TextChanged += new System.EventHandler(this.txtAmount_TextChanged);
      // 
      // label2
      // 
      this.label2.Dock = System.Windows.Forms.DockStyle.Left;
      this.label2.Location = new System.Drawing.Point(12, 12);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(78, 21);
      this.label2.TabIndex = 6;
      this.label2.Text = "Amount:";
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.cboRType);
      this.panel1.Controls.Add(this.label1);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel1.Location = new System.Drawing.Point(2, 2);
      this.panel1.Name = "panel1";
      this.panel1.Padding = new System.Windows.Forms.Padding(12);
      this.panel1.Size = new System.Drawing.Size(261, 45);
      this.panel1.TabIndex = 0;
      // 
      // cboRType
      // 
      this.cboRType.Dock = System.Windows.Forms.DockStyle.Right;
      this.cboRType.FormattingEnabled = true;
      this.cboRType.Location = new System.Drawing.Point(117, 12);
      this.cboRType.Margin = new System.Windows.Forms.Padding(3, 15, 3, 3);
      this.cboRType.Name = "cboRType";
      this.cboRType.Size = new System.Drawing.Size(132, 23);
      this.cboRType.TabIndex = 5;
      this.cboRType.SelectedIndexChanged += new System.EventHandler(this.cboType_SelectedIndexChanged);
      this.cboRType.ValueMemberChanged += new System.EventHandler(this.cboRType_ValueMemberChanged_1);
      // 
      // label1
      // 
      this.label1.Dock = System.Windows.Forms.DockStyle.Left;
      this.label1.Location = new System.Drawing.Point(12, 12);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(78, 21);
      this.label1.TabIndex = 4;
      this.label1.Text = "Options:";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // panel6
      // 
      this.panel6.Controls.Add(this.label3);
      this.panel6.Controls.Add(this.cboAccType);
      this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel6.Location = new System.Drawing.Point(2, 47);
      this.panel6.Name = "panel6";
      this.panel6.Padding = new System.Windows.Forms.Padding(12);
      this.panel6.Size = new System.Drawing.Size(261, 45);
      this.panel6.TabIndex = 6;
      // 
      // label3
      // 
      this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
      this.label3.Location = new System.Drawing.Point(12, 12);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(105, 21);
      this.label3.TabIndex = 4;
      this.label3.Text = "AccountType:";
      this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // cboAccType
      // 
      this.cboAccType.Dock = System.Windows.Forms.DockStyle.Right;
      this.cboAccType.FormattingEnabled = true;
      this.cboAccType.Location = new System.Drawing.Point(117, 12);
      this.cboAccType.Margin = new System.Windows.Forms.Padding(3, 15, 3, 3);
      this.cboAccType.Name = "cboAccType";
      this.cboAccType.Size = new System.Drawing.Size(132, 23);
      this.cboAccType.TabIndex = 5;
      this.cboAccType.SelectedIndexChanged += new System.EventHandler(this.cboAccType_SelectedIndexChanged);
      // 
      // panel7
      // 
      this.panel7.Controls.Add(this.txtRate);
      this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel7.Location = new System.Drawing.Point(2, 2);
      this.panel7.Margin = new System.Windows.Forms.Padding(2);
      this.panel7.Name = "panel7";
      this.panel7.Padding = new System.Windows.Forms.Padding(2);
      this.panel7.Size = new System.Drawing.Size(261, 45);
      this.panel7.TabIndex = 7;
      // 
      // txtRate
      // 
      this.txtRate.Dock = System.Windows.Forms.DockStyle.Fill;
      this.txtRate.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
      this.txtRate.Location = new System.Drawing.Point(2, 2);
      this.txtRate.Name = "txtRate";
      this.txtRate.Padding = new System.Windows.Forms.Padding(12);
      this.txtRate.Size = new System.Drawing.Size(257, 41);
      this.txtRate.TabIndex = 6;
      this.txtRate.Text = "Interest Rate:";
      this.txtRate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // pnResult
      // 
      this.pnResult.Controls.Add(this.panel5);
      this.pnResult.Controls.Add(this.panel4);
      this.pnResult.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnResult.Location = new System.Drawing.Point(2, 32);
      this.pnResult.Name = "pnResult";
      this.pnResult.Size = new System.Drawing.Size(560, 134);
      this.pnResult.TabIndex = 1;
      this.pnResult.Visible = false;
      // 
      // panel5
      // 
      this.panel5.Controls.Add(this.txtResult);
      this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel5.Location = new System.Drawing.Point(0, 41);
      this.panel5.Name = "panel5";
      this.panel5.Padding = new System.Windows.Forms.Padding(25, 5, 5, 5);
      this.panel5.Size = new System.Drawing.Size(560, 93);
      this.panel5.TabIndex = 4;
      // 
      // txtResult
      // 
      this.txtResult.Dock = System.Windows.Forms.DockStyle.Fill;
      this.txtResult.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
      this.txtResult.Location = new System.Drawing.Point(25, 5);
      this.txtResult.Name = "txtResult";
      this.txtResult.Size = new System.Drawing.Size(530, 83);
      this.txtResult.TabIndex = 3;
      this.txtResult.Text = "Result";
      // 
      // panel4
      // 
      this.panel4.Controls.Add(this.label11);
      this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel4.Location = new System.Drawing.Point(0, 0);
      this.panel4.Name = "panel4";
      this.panel4.Padding = new System.Windows.Forms.Padding(25, 10, 5, 5);
      this.panel4.Size = new System.Drawing.Size(560, 41);
      this.panel4.TabIndex = 2;
      // 
      // label11
      // 
      this.label11.Dock = System.Windows.Forms.DockStyle.Top;
      this.label11.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
      this.label11.Location = new System.Drawing.Point(25, 10);
      this.label11.Margin = new System.Windows.Forms.Padding(20, 0, 3, 0);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(530, 24);
      this.label11.TabIndex = 8;
      this.label11.Text = "Result:";
      this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // panel3
      // 
      this.panel3.Controls.Add(this.btnCalculate);
      this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel3.Location = new System.Drawing.Point(2, 2);
      this.panel3.Name = "panel3";
      this.panel3.Padding = new System.Windows.Forms.Padding(26, 3, 2, 3);
      this.panel3.Size = new System.Drawing.Size(560, 30);
      this.panel3.TabIndex = 0;
      // 
      // btnCalculate
      // 
      this.btnCalculate.BackColor = System.Drawing.SystemColors.Control;
      this.btnCalculate.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnCalculate.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
      this.btnCalculate.Location = new System.Drawing.Point(26, 3);
      this.btnCalculate.Name = "btnCalculate";
      this.btnCalculate.Size = new System.Drawing.Size(80, 24);
      this.btnCalculate.TabIndex = 0;
      this.btnCalculate.Text = "Calculate";
      this.btnCalculate.UseVisualStyleBackColor = false;
      this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
      // 
      // FormInterestRate
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
      this.ClientSize = new System.Drawing.Size(584, 316);
      this.Controls.Add(this.splitContainer1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.MaximizeBox = false;
      this.Name = "FormInterestRate";
      this.Padding = new System.Windows.Forms.Padding(10);
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Interate Rate Calculator";
      this.splitContainer1.Panel1.ResumeLayout(false);
      this.splitContainer1.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
      this.splitContainer1.ResumeLayout(false);
      this.splitContainer3.Panel1.ResumeLayout(false);
      this.splitContainer3.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
      this.splitContainer3.ResumeLayout(false);
      this.panel2.ResumeLayout(false);
      this.panel2.PerformLayout();
      this.panel1.ResumeLayout(false);
      this.panel6.ResumeLayout(false);
      this.panel7.ResumeLayout(false);
      this.pnResult.ResumeLayout(false);
      this.panel5.ResumeLayout(false);
      this.panel4.ResumeLayout(false);
      this.panel3.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private global::System.Windows.Forms.SplitContainer splitContainer1;
    private global::System.Windows.Forms.SplitContainer splitContainer3;
    private global::System.Windows.Forms.Panel panel2;
    private NumericTextBox txtAmount;
    private global::System.Windows.Forms.Label label2;
    private global::System.Windows.Forms.Panel panel1;
    private global::System.Windows.Forms.ComboBox cboRType;
    private global::System.Windows.Forms.Label label1;
    private global::System.Windows.Forms.Panel pnResult;
    private global::System.Windows.Forms.Label txtResult;
    private global::System.Windows.Forms.Panel panel4;
    private global::System.Windows.Forms.Label label11;
    private global::System.Windows.Forms.Panel panel3;
    private global::System.Windows.Forms.Button btnCalculate;
    private System.Windows.Forms.Panel panel5;
    private System.Windows.Forms.Panel panel6;
    private System.Windows.Forms.ComboBox cboAccType;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Panel panel7;
    private System.Windows.Forms.Label txtRate;
  }
}