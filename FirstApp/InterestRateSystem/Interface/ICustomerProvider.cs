﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterestRateSystem.Interface
{
  public interface ICustomerProvider
  {
    IRate getRateByMonth(int month);
    IBonus getRateBonusBigAmount(int month, float amount);
  }
}
