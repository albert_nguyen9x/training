﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterestRateSystem.Interface
{
  public interface ICustomer
  {
    float Rating();
  }
  
}
