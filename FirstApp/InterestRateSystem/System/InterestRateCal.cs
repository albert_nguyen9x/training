﻿using InterestRateSystem.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterestRateSystem.System
{
  public class InterestRateCal
  {
    private static InterestRateCal _instance = new InterestRateCal();
    public static InterestRateCal Instance() => _instance;

    ICustomerProvider customerProvider = null;
    public void AttachCustomerProvider(ICustomerProvider mCustomerProvider)
    {
      customerProvider = mCustomerProvider;
    }
    public (float IRateAmount, float BBigAmount, float BonusOther, float Total) Calculate(ICustomer customer, int month, float amount)
    {
      if (customerProvider == null) throw new NullReferenceException("Servics provider not attached! Attach before using System");
      IRate interestRate = customerProvider.getRateByMonth(month);
      float interestRateAmount = interestRate.Rating() * amount;

      IBonus bonusBig = customerProvider.getRateBonusBigAmount(month, amount);
      float bonusBigAmount = bonusBig!=null?bonusBig.Rating() * amount:0;

      float bonusOther = customer.Rating();
      float bonusCustomer = amount * bonusOther / 100;
      
      float Total = amount + interestRateAmount + bonusBigAmount + bonusCustomer;
      return (IRateAmount: interestRateAmount, BBigAmount: bonusBigAmount, BonusOther: bonusCustomer, Total: Total);
    }

    //@Deprecate
    /*private static void findLowerBinary(List<Bonus> data, int start, int end, ref float cBonus, float amount)
    {
      if (start < end)
      {
        int mid = (start + end) / 2;
        if (data[mid].Amount <= amount)
        {
          cBonus = data[mid].BonusRate;
          findLowerBinary(data, mid+1, end, ref cBonus, amount);
        }
        else
        {
          findLowerBinary(data, start, mid-1, ref cBonus, amount);
        }
      }
      else if (start == end)
      {
        if (data[start].Amount <= amount)
        {
          cBonus = data[start].BonusRate;
        }
      }
    }*/
  }
}
