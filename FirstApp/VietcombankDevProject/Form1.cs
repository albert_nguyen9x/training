﻿using InterestRateSystem.Interface;
using InterestRateSystem.System;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VietcombankDevProject
{
  public partial class Form1 : Form
  {
    public Form1()
    {
      InitializeComponent();
    }

    private void button1_Click(object sender, EventArgs e)
    {
      //Test API :
      InterestRateCal.Instance().AttachCustomerProvider(new VCBInfoProvider());

      (_,_,_, float total) = InterestRateCal.Instance().Calculate(new PersonalCustomer(), 3, 100);
      MessageBox.Show($"total amount will bereceived :{total}$");
    }
    class VCBInfoProvider : ICustomerProvider
    {
      public IBonus getRateBonusBigAmount(int month, float amount)
      {
        return new VCBBonus();
      }

      public IRate getRateByMonth(int month)
      {
        return new VCBRate() { InterestRate = 0.01f };
      }
    }
  }
}
