﻿using InterestRateSystem.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VietcombankDevProject
{
  internal abstract class VCBCustomer:SuperBaseClass,ICustomer
  {
    public float Rating() => VCBRatePercent();

    public abstract int VCBRatePercent();
  }

  internal class CompanyCustomer : VCBCustomer
  {
    public override int VCBRatePercent()
    {
      return 1;
    }
  }

  internal class PersonalCustomer : VCBCustomer
  {
    public override int VCBRatePercent()
    {
      return 0;
    }
  }
  internal class CoperativeBankCustomer : VCBCustomer
  {
    public override int VCBRatePercent()
    {
      return 2;
    }
  }
}

