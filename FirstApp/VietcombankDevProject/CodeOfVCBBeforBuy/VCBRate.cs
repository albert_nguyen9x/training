﻿using InterestRateSystem.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VietcombankDevProject
{
  internal class VCBRate: SuperBaseClass, IRate
  {
    public int Month { get; set; }
    public float InterestRate { get; set; }

    public float Rating() => InterestRate;
  }
}
