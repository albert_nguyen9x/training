﻿using InterestRateSystem.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VietcombankDevProject
{

  internal class SuperBaseClass
  {

  }
  class VCBBonus:SuperBaseClass,IBonus
  {
      public float Amount { get; set; }
      public int Month { get; set; }
      public float BonusRate { get; set; }

    public float Rating() => BonusRate;
  }
}
