﻿using InterestRateSystem.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechcombankDevProject
{
  internal abstract class TCBCustomer:SuperBaseClass,ICustomer
  {
    public abstract int TCBRatePercent();
    public float Rating() => TCBRatePercent();

  }

  internal class CompanyCustomer : TCBCustomer
  {
    public override int TCBRatePercent()
    {
      return 1;
    }
  }

  internal class PersonalCustomer : TCBCustomer
  {
    public override int TCBRatePercent()
    {
      return 0;
    }
  }
  internal class CoperativeBankCustomer : TCBCustomer
  {
    public override int TCBRatePercent()
    {
      return 2;
    }
  }
}

