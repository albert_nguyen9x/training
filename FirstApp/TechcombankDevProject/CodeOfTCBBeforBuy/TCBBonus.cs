﻿using InterestRateSystem.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechcombankDevProject
{

  internal class SuperBaseClass
  {

  }
  class TCBBonus:SuperBaseClass,IBonus
  {
      public float Amount { get; set; }
      public int Month { get; set; }
      public float BonusRate { get; set; }

    public float Rating() => BonusRate;
    
  }
}
